class Api {
  constructor(key) {
    this.apiKey = key;
  }
  async obtenerApi() {
    const url = await fetch(
      `https://app.ticketmaster.com/discovery/v2/events.json?apikey=${this.apiKey}`
    );
    const urlJson = await url.json();
    return {
      urlJson,
    };
  }
 
}
