class Interfaz {
  constructor() {
    this.init();
  }

  init() {
    this.iniciarMetodoApi();
  }

  iniciarMetodoApi() {
    api.obtenerApi().then((res) => {
      const resultado = res.urlJson._embedded.events;
      this.configuracionRandon(resultado);
    });
  }

  configuracionRandon(res) {
    let iE = new Set();
    let pE = [];
    let pCs;
    let i, r;
    // enumera por cada itens disponibles
    for (i = 0; i < res.length; i++) pE[i] = i;

    for (i = 0; i <= 10; i++) {
      // crear los numeros en randon con la salida en variables r limite 10 numeros
      r = Math.floor(Math.random() * pE.length);
      // cambio a new set obtiene los res randon seleccionado
      // elimina los duplicados
      iE.add(res[pE[r]]);
      // cambio a un arreglo
      let iRaU = Array.from(iE);
      //elimina undefined
      for (let e = 0; e < iRaU.length; e++) {
        if (iRaU[e] === undefined) iRaU.splice(e, 1);
      }
      pCs = iRaU;
    }
    // solo 3 posiciones salida al azar
    this.mostrarElementosSalida(pCs.slice(0, 3));
  }
  mostrarElementosSalida(res) {
    const sectTicket = document.getElementById("card");
    const div = document.createElement("div");

    let imagen;
    div.classList.add("card-row");
    res.forEach((elemento) => {
      elemento.images.forEach((element) => {
        if (element.width === 1024) imagen = element.url;
      });

      div.innerHTML += `
        <div class="card-contet">
          <img src="${imagen}" alt="No Foto"/>
          <ul>
            <li><i class="fas fa-bullhorn"></i>  <span>${elemento.type}</Tipo:></li>
            <li><i class="fas fa-layer-group"></i>   <span>${elemento.name}</Nombre:></li>
            <li><i class="fas fa-flag-usa" ></i>   <span>${elemento.locale}</Localidad:></li>
            <li>
              <span>
                <i class="far fa-calendar-alt"></i>  ${elemento.dates.start.localDate}</Fecha:>
              </span>     
              <span>
                <i class="far fa-hourglass" style="margin-left:1rem;"></i>  ${elemento.dates.start.localTime}</Hora:>
              </span>   
              </li>
            <li><a href="${elemento.url}"  target="_black">Read More</a></li>
          </ul>
        </div>
      `;
    });
    //  agregamos  a el template
    sectTicket.appendChild(div);
  }
}
