const navContent = document.getElementById("nav-content");
const navMenu = document.getElementById("nav-menu");

// despliege
navMenu.addEventListener("click", () => {
  navContent.classList.toggle("nav-toggle");
});

// boton
function myFunction(x) {
  x.classList.toggle("change");
}

window.onscroll = function () {
  scrollFunction();
};
function scrollFunction() {
  let scroll =
    document.body.scrollTop > 80 || document.documentElement.scrollTop;
  const nav = document.getElementById("nav");

  scroll
    ? (nav.style.backgroundColor = "#333333")
    : (nav.style.backgroundColor = "#84b84c");
}
